# Time logging

This is my pet project to keep me motivated to log time at work.

The script gives you a percentage of the logged work for the current day for the user passed as an argument. It also prints the amount of tasks (worklogs) and the total logged time for the current day.

first arg is the jira nickname of the user you want to monitor
second arg is the session cookie for your active jira session. Note: if you change to an elevated admin session, your session cookie changes and the script will not run.

### Example
```willemdauwen$ python3 tempo.py willem 4B3910A3F3A9437B60BD01938B04804B```

![alt text](http://files.uitpas.be/misc/terminal.png "terminal")

Kudos to those who get the C. Coulson refference ;-)

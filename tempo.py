import time
import requests
import json
from math import trunc
from sparklines import sparklines
import datetime
from time import gmtime, strftime
import pyfiglet
import sys

#color classes
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

#banner
ascii_banner = bcolors.HEADER + pyfiglet.figlet_format("my log does not judge") + bcolors.ENDC

#input
today = datetime.date.today()
usr = sys.argv[1]

headers = {
    'Cookie': 'JSESSIONID=' + sys.argv[2]
}

#API call
response = requests.get('https://jira.uitdatabank.be/rest/tempo-timesheets/3/worklogs/?username=' + usr + '&dateFrom='+ today.strftime("%Y-%m-%d") + '&tempoApiToken=b17538af-34ed-4dab-8883-c810fb651233', headers= headers )
reply = response.text

#data wrangling
lijst1 = []
lijst2 = []
data = json.loads(reply)
for data in data:
	lijst1.append(data["timeSpentSeconds"])
	lijst2.append(data["comment"])

#percentage calculation
part = sum(lijst1)
whole = 27360

def percentage(part, whole):
	return 100 *float(part)/float(whole)

#printing things to terminal

print("\n-------------------------------------------------------------------------------------")
print(ascii_banner)
print("Het is nu " + bcolors.WARNING + strftime("%H:%M") + bcolors.ENDC + " en je logde al " + bcolors.OKGREEN +str(trunc(percentage(part, whole))) + bcolors.ENDC + " procent van je werkdag \n")

for line in sparklines(lijst1):
	print(bcolors.OKGREEN + line + bcolors.ENDC)

print("Aantal taken so far: " + bcolors.OKGREEN + str(len(lijst1))+ bcolors.ENDC) 
print("Aantal gelogde uren: " + bcolors.HEADER + str(format((part * 0.000277777778), '.2f')) + bcolors.ENDC)
print("\n-------------------------------------------------------------------------------------")